package internship.storage.manager.test;


import internship.storage.StoreApplication;
import internship.storage.controller.ManagerController;
import internship.storage.dto.ManagerDTO;
import internship.storage.model.ManagerEntity;
import internship.storage.repository.ManagerRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
@ContextConfiguration(classes = {StoreApplication.class, ManagerController.class})
public class ManagerEntityControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MockMvc mockMvc;

    @Autowired
    ManagerRepository managerRepository;
    @Autowired
    ManagerController managerController;


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }
    @Before
    public void setUp() {

        this.mockMvc = standaloneSetup(managerController)
                .defaultRequest(get("/")
                .accept(MediaType.APPLICATION_JSON))
                .build();
    }

    @After
    public void tearDown() {

        managerRepository.deleteAll();
    }


    @Test
    public void getAllManagersTest() throws Exception{

        //PREPARE
        managerRepository.save(new ManagerEntity("Jack", "Bauer"));
        managerRepository.save(new ManagerEntity("Chloe", "O'Brian"));

        //EXECUTE
        mockMvc.perform(get("/managers/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    public void getAllManagersRequestEmptyRepository() throws Exception{


        //EXECUTE
        mockMvc.perform(get("/managers/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteAllManagers() throws Exception{


        //PREPARE
        managerRepository.save(new ManagerEntity("Jack", "Bauer"));
        managerRepository.save(new ManagerEntity("Chloe", "O'Brian"));

        //EXECUTE
        mockMvc.perform(delete("/managers/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void deleteAllManagersEmptyList() throws Exception{



        //EXECUTE
        mockMvc.perform(delete("/managers/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void getManager() throws Exception {

        //PREPARE
        String stringResponse;
        ManagerEntity managerEntity = new ManagerEntity("Jack", "Bauer");
        managerRepository.save(managerEntity);
        long id = managerEntity.getId();


        //EXECUTE
        ResultActions result = mockMvc.perform(get("/managers/{0}",id).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void getNonExistentManager() throws Exception {

        //EXECUTE
      mockMvc.perform(get("/managers/{0}",4).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    @Test
    public void postManager() throws Exception{

        //PREPARE
         ManagerDTO managerDTO = new ManagerDTO("Jack", "Bauer");
        //EXECUTE

        mockMvc.perform(post("/managers/")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(json(managerDTO)))
                  .andDo(print())
                  .andExpect(status().isCreated());
    }


    @Test
    public void postExistingManager() throws Exception{

        //PREPARE
        ManagerEntity managerEntity = new ManagerEntity("Jack", "Bauer");
        managerRepository.save(managerEntity);
        ManagerDTO managerDTO = new ManagerDTO("Jack", "Bauer");


        //EXECUTE
        mockMvc.perform(post("/managers/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(managerDTO)))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void updateManager() throws  Exception{

        //PREPARE
        ManagerEntity managerEntity = new ManagerEntity("Jack", "Bauer");
        ManagerEntity m = new ManagerEntity("Jack-Marvin", "Bauer");

        managerRepository.save(managerEntity);
        m.setId(managerEntity.getId());

        //EXECUTE
       mockMvc.perform(put("/managers/{0}",m.getId())
                .contentType(contentType).content(json((m))))
                .andDo(print())
                .andExpect(status().isOk());

    }



    @Test
    public void updateNonExistentManager() throws  Exception{

        //PREPARE
        ManagerEntity managerEntity = new ManagerEntity("Jack", "Bauer");

        //EXECUTE
         mockMvc.perform(post("/managers/{0}",4)
                .accept(contentType).content(json((managerEntity))))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteManager() throws Exception{
        //PREPARE
        ManagerEntity managerEntity = new ManagerEntity("Jack", "Bauer");
        ManagerEntity m = new ManagerEntity("Jack", "Bauer");

        managerRepository.save(managerEntity);
        m.setId(managerEntity.getId());


        //EXECUTE
        mockMvc.perform(delete("/managers/{0}", managerEntity.getId()).contentType(contentType))
                .andDo(print())
                .andExpect(status().isNoContent());
    }


    @Test
    public void deleteNonExistentManager() throws Exception{
         //EXECUTE
        mockMvc.perform(delete("/managers/{0}",2).contentType(contentType))
                .andDo(print())
                .andExpect(status().isNotFound());
    }







    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }



}
