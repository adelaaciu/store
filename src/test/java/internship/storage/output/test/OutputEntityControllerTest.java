package internship.storage.output.test;

import internship.storage.StoreApplication;
import internship.storage.controller.OutputController;
import internship.storage.controller.StockController;
import internship.storage.model.OutputEntity;
import internship.storage.model.ProductEntity;
import internship.storage.model.StoreEntity;
import internship.storage.repository.InputRepository;
import internship.storage.repository.OutputRepository;
import internship.storage.repository.ProductRepository;
import internship.storage.repository.StoreRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
@ContextConfiguration(classes = {StoreApplication.class})
public class OutputEntityControllerTest {



    private MockMvc mockMvc;
    @Autowired
    InputRepository inputRepository;
    @Autowired
    OutputRepository outputRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    StockController stockController;
    @Autowired
    OutputController outputController;


    @Before
    public void setUp(){
        this.mockMvc = standaloneSetup(outputController)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON))
                .build();

    }

    @After
    public void tearDown() {
        inputRepository.deleteAll();
        outputRepository.deleteAll();
        productRepository.deleteAll();
        storeRepository.deleteAll();
    }


    @Test
    public  void  getAllOutputs() throws  Exception{

        StoreEntity store = new StoreEntity("shop1");
        storeRepository.save(store);
        ProductEntity productEntity = new ProductEntity("product1");

        productRepository.save(productEntity);

        outputRepository.save(new OutputEntity(productEntity, store, 15L));

        mockMvc.perform(get("/outputs/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public  void  getNonExistentOutputs() throws  Exception{

        mockMvc.perform(get("/outputs/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }



    @Test
    public  void  getOutput() throws  Exception{

        StoreEntity store = new StoreEntity("shop1");
        storeRepository.save(store);
        ProductEntity productEntity = new ProductEntity("product1");
        productRepository.save(productEntity);
        OutputEntity out = new OutputEntity(productEntity,  store, 15L);
        outputRepository.save(out);


        mockMvc.perform(get("/outputs/{0}",out.getId()).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public  void  getNonExistentOutput() throws  Exception{

        mockMvc.perform(get("/outputs/{0}",3).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }




    @Test
    public void addOutputProdNotFound() throws Exception {


        StoreEntity store = new StoreEntity("shop");
        ProductEntity productEntity = new ProductEntity("productEntity");
        productEntity.setId(0);

        storeRepository.save(store);

        OutputEntity outputEntity = new OutputEntity(productEntity, storeRepository.findOne(store.getId()), 15L);

        mockMvc.perform(post("/outputs/")
                .content(outputEntity.toJSon(outputEntity))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void addOutputStoreNotFound() throws Exception {


        StoreEntity store = new StoreEntity("shop");
        ProductEntity productEntity = new ProductEntity("productEntity");
        productRepository.save(productEntity);

        OutputEntity outputEntity = new OutputEntity(productRepository.findOne(productEntity.getId()),  store, 15L);

        mockMvc.perform(post("/outputs/")
                .content(outputEntity.toJSon(outputEntity))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }


    @Test
    public void putOutput() throws Exception{

        mockMvc.perform(put("/outputs/{id}",0))
                .andExpect(status().isMethodNotAllowed());

    }


    @Test
    public void deleteOutput() throws Exception{

        mockMvc.perform(delete("/outputs/{id}",0))
                .andExpect(status().isMethodNotAllowed());

    }

    @Test
    public void deleteAllOutputs() throws Exception{

        mockMvc.perform(delete("/outputs/"))
                .andExpect(status().isMethodNotAllowed());

    }




}
