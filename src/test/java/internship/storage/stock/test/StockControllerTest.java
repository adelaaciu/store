package internship.storage.stock.test;

import internship.storage.StoreApplication;
import internship.storage.controller.StockController;
import internship.storage.model.InputEntity;
import internship.storage.model.OutputEntity;
import internship.storage.model.ProductEntity;
import internship.storage.model.StoreEntity;
import internship.storage.repository.InputRepository;
import internship.storage.repository.OutputRepository;
import internship.storage.repository.ProductRepository;
import internship.storage.repository.StoreRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by internship on 15.07.2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
@ContextConfiguration(classes = {StoreApplication.class,StockController.class})
public class StockControllerTest {


    private MockMvc mockMvc;
    @Autowired
    InputRepository inputRepository;
    @Autowired
    OutputRepository outputRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    StockController stockController;

    @Before
    public void setUp(){
        this.mockMvc = standaloneSetup(stockController)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON))
                .build();

    }


    @Test
    public void getStock() throws Exception {


        StoreEntity store = new StoreEntity("Fruits and Vegetables");

        storeRepository.save(store);

        ProductEntity productEntity = new ProductEntity("apple");

        productRepository.save(productEntity);

        inputRepository.save(new InputEntity(productRepository.findOne(productEntity.getId()),  store, 15L));

        outputRepository.save(new OutputEntity(productRepository.findOne(productEntity.getId()), store, 6L));


        mockMvc.perform(get("/stocks").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());


    }


}
