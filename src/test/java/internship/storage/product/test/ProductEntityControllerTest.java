package internship.storage.product.test;


import internship.storage.StoreApplication;
import internship.storage.controller.ProductController;
import internship.storage.model.ProductEntity;
import internship.storage.repository.ProductRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
@ContextConfiguration(classes = {StoreApplication.class, ProductController.class})
public class ProductEntityControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MockMvc mockMvc;

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductController productController;


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }
    @Before
    public void setUp() {

        this.mockMvc = standaloneSetup(productController)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON))
                .build();
    }

    @After
    public void tearDown() {

        productRepository.deleteAll();
    }



    @Test
    public void getAllProducts() throws Exception{

        //PREPARE
        productRepository.save(new ProductEntity("Jack"));
        productRepository.save(new ProductEntity("Chloe"));

        //EXECUTE
        mockMvc.perform(get("/products/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllProductsRequestEmptyRepository() throws Exception{


        //EXECUTE
        mockMvc.perform(get("/products/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteAllProducts() throws Exception{


        //PREPARE
        productRepository.save(new ProductEntity("Jack"));
        productRepository.save(new ProductEntity("Chloe"));

        //EXECUTE
        mockMvc.perform(delete("/products/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void deleteAllProductsEmptyList() throws Exception{

        //EXECUTE
        mockMvc.perform(delete("/products/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void getProduct() throws Exception {

        //PREPARE
        String stringResponse;
        ProductEntity productEntity = new ProductEntity("Jack");
        productRepository.save(productEntity);
        long id = productEntity.getId();

        //EXECUTE
        ResultActions result = mockMvc.perform(get("/products/{0}",id).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void getNonExistentProduct() throws Exception {

        //EXECUTE
        mockMvc.perform(get("/products/{0}",4).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    @Test
    public void postProduct() throws Exception{

        //PREPARE
        ProductEntity productEntity = new ProductEntity("Jack");
        //EXECUTE
        mockMvc.perform(post("/products/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(productEntity)))
                .andDo(print())
                .andExpect(status().isCreated());
    }


    @Test
    public void postExistingProduct() throws Exception{

        //PREPARE
        ProductEntity productEntity = new ProductEntity("Jack");
        productRepository.save(productEntity);
        ProductEntity m = new ProductEntity("Jack");


        //EXECUTE
        mockMvc.perform(post("/products/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(m)))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void updateProduct() throws  Exception{

        //PREPARE
        ProductEntity productEntity = new ProductEntity("Jack");
        ProductEntity m = new ProductEntity("Jack-Marvin");

        productRepository.save(productEntity);
        m.setId(productEntity.getId());

        //EXECUTE
        mockMvc.perform(put("/products/{0}",m.getId())
                .contentType(contentType).content(json((m))))
                .andDo(print())
                .andExpect(status().isOk());

    }



    @Test
    public void updateNonExistentProduct() throws  Exception{

        //PREPARE
        ProductEntity productEntity = new ProductEntity("Jack");

        //EXECUTE
        mockMvc.perform(put("/products/{0}",4)
                .contentType(contentType).content(json((productEntity))))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteProduct() throws Exception{
        //PREPARE
        ProductEntity productEntity = new ProductEntity("Jack");
        ProductEntity m = new ProductEntity("Jack");

        productRepository.save(productEntity);
        m.setId(productEntity.getId());


        //EXECUTE
        mockMvc.perform(delete("/products/{0}", productEntity.getId()).contentType(contentType))
                .andDo(print())
                .andExpect(status().isNoContent());
    }


    @Test
    public void deleteNonExistentProduct() throws Exception{
        //EXECUTE
        mockMvc.perform(delete("/products/{0}",2).contentType(contentType))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
