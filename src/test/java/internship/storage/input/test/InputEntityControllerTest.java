package internship.storage.input.test;

import internship.storage.*;
import internship.storage.controller.InputController;
import internship.storage.controller.StockController;
import internship.storage.model.InputEntity;
import internship.storage.model.ProductEntity;
import internship.storage.model.StoreEntity;
import internship.storage.repository.InputRepository;
import internship.storage.repository.OutputRepository;
import internship.storage.repository.ProductRepository;
import internship.storage.repository.StoreRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by internship on 15.07.2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
@ContextConfiguration(classes = {StoreApplication.class})
public class InputEntityControllerTest {


    private MockMvc mockMvc;

    @Autowired
    InputRepository inputRepository;
    @Autowired
    OutputRepository outputRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    StockController stockController;
    @Autowired
    InputController inputController;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Before
    public void setUp() {
        this.mockMvc = standaloneSetup(inputController)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON))
                .build();

    }


    @After
    public void tearDown() {
        inputRepository.deleteAll();
        outputRepository.deleteAll();
        productRepository.deleteAll();
        storeRepository.deleteAll();
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Test
    public void getAllInputs() throws Exception {

        StoreEntity store = new StoreEntity("shop1");
        storeRepository.save(store);
        ProductEntity productEntity = new ProductEntity("product1");

        productRepository.save(productEntity);

        inputRepository.save(new InputEntity(productRepository.findOne(productEntity.getId()), store, 15L));

        mockMvc.perform(get("/inputs/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void getNonExistentInputs() throws Exception {

        mockMvc.perform(get("/inputs/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void getInput() throws Exception {

        StoreEntity store = new StoreEntity("shop1");
        storeRepository.save(store);
        ProductEntity productEntity = new ProductEntity("product1");
        productRepository.save(productEntity);
        InputEntity inputEntity = new InputEntity(productRepository.findOne(productEntity.getId()), store, 15L);
        inputRepository.save(inputEntity);

        mockMvc.perform(get("/inputs/{0}", inputEntity.getId()).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getNonExistentInput() throws Exception {

        mockMvc.perform(get("/inputs/{0}", 3).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void addInput() throws Exception {


        StoreEntity store = new StoreEntity("shop");
        ProductEntity productEntity = new ProductEntity("productEntity");

        storeRepository.save(store);
        productRepository.save(productEntity);

        InputEntity inputEntity = new InputEntity(productRepository.findOne(productEntity.getId()), storeRepository.findOne(store.getId()), 15L);

        mockMvc.perform(post("/inputs/")
                .content(inputEntity.toJSon(inputEntity))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void addInputProdNotFound() throws Exception {


        StoreEntity store = new StoreEntity("shop");
        ProductEntity productEntity = new ProductEntity("productEntity");
        productEntity.setId(0);

        storeRepository.save(store);

        InputEntity inputEntity = new InputEntity(productEntity,  storeRepository.findOne(store.getId()), 15L);

        mockMvc.perform(post("/inputs/")
                .content(inputEntity.toJSon(inputEntity))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void addInputStoreNotFound() throws Exception {


        StoreEntity store = new StoreEntity("shop");
        ProductEntity productEntity = new ProductEntity("productEntity");
        productRepository.save(productEntity);

        InputEntity inputEntity = new InputEntity(productRepository.findOne(productEntity.getId()), store, 15L);

        mockMvc.perform(post("/inputs/")
                .content(inputEntity.toJSon(inputEntity))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }


    @Test
    public void putInput() throws Exception{

        mockMvc.perform(put("/inputs/{id}",0))
                .andExpect(status().isMethodNotAllowed());

    }


    @Test
    public void deleteInput() throws Exception{

        mockMvc.perform(delete("/inputs/{id}",0))
                .andExpect(status().isMethodNotAllowed());

    }

    @Test
    public void deleteAllInputs() throws Exception{

        mockMvc.perform(delete("/inputs/"))
                .andExpect(status().isMethodNotAllowed());

    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


}

