package internship.storage.store.test;


import internship.storage.StoreApplication;
import internship.storage.controller.StoreController;
import internship.storage.model.StoreEntity;
import internship.storage.repository.StoreRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
@ContextConfiguration(classes = {StoreApplication.class, StoreController.class})
public class StoreControllerTest {


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MockMvc mockMvc;

    @Autowired
    StoreRepository storeRepository;
    @Autowired
    StoreController storeController;


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }
    @Before
    public void setUp() {

        this.mockMvc = standaloneSetup(storeController)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON))
                .build();
    }

    @After
    public void tearDown() {

        storeRepository.deleteAll();
    }



    @Test
    public void getAllStores() throws Exception{

        //PREPARE
        storeRepository.save(new StoreEntity("Jack"));
        storeRepository.save(new StoreEntity("Chloe"));

        //EXECUTE
        mockMvc.perform(get("/stores/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllStoresRequestEmptyRepository() throws Exception{


        //EXECUTE
        mockMvc.perform(get("/stores/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteAllStores() throws Exception{


        //PREPARE
        storeRepository.save(new StoreEntity("Jack"));
        storeRepository.save(new StoreEntity("Chloe"));

        //EXECUTE
        mockMvc.perform(delete("/stores/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void deleteAllStoresEmptyList() throws Exception{

        //EXECUTE
        mockMvc.perform(delete("/stores/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void getStore() throws Exception {

        //PREPARE
        String stringResponse;
        StoreEntity store = new StoreEntity("Jack");
        storeRepository.save(store);

        long id = store.getId();

        //EXECUTE
        ResultActions result = mockMvc.perform(get("/stores/{0}",id).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void getNonExistentStore() throws Exception {

        //EXECUTE
        mockMvc.perform(get("/stores/{0}",4).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    @Test
    public void postStore() throws Exception{

        //PREPARE
        StoreEntity store = new StoreEntity("Jack");
        //EXECUTE
        mockMvc.perform(post("/stores/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(store)))
                .andDo(print())
                .andExpect(status().isCreated());
    }


    @Test
    public void postExistingStore() throws Exception{

        //PREPARE
        StoreEntity store = new StoreEntity("Jack");
        storeRepository.save(store);

        StoreEntity m = new StoreEntity("Jack");



        //EXECUTE
        mockMvc.perform(post("/stores/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(m)))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void updateStore() throws  Exception{

        //PREPARE
        StoreEntity store= new StoreEntity("Jack");
        StoreEntity m = new StoreEntity("Jack-Marvin");

        storeRepository.save(store);
        m.setId(store.getId());

        //EXECUTE
        mockMvc.perform(put("/stores/{0}",store.getId())
                .contentType(contentType).content(json((m))))
                .andDo(print())
                .andExpect(status().isOk());

    }



    @Test
    public void updateNonExistentStore() throws  Exception{

        //PREPARE
        StoreEntity store = new StoreEntity("Jack");

        //EXECUTE
        mockMvc.perform(put("/stores/{0}",4)
                .contentType(contentType).content(json((store))))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteStore() throws Exception{
        //PREPARE
        StoreEntity store = new StoreEntity("Jack");
        StoreEntity m = new StoreEntity("Jack");

        storeRepository.save(store);
        m.setId(store.getId());


        //EXECUTE
        mockMvc.perform(delete("/stores/{0}",store.getId()).contentType(contentType))
                .andDo(print())
                .andExpect(status().isNoContent());
    }


    @Test
    public void deleteNonExistentStore() throws Exception{
        //EXECUTE
        mockMvc.perform(delete("/stores/{0}",2).contentType(contentType))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
