package internship.storage.controller;

import internship.storage.model.InputEntity;
import internship.storage.model.OutputEntity;
import internship.storage.model.ProductEntity;
import internship.storage.pojo.StockProduct;
import internship.storage.repository.InputRepository;
import internship.storage.repository.OutputRepository;
import internship.storage.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Created by internship on 14.07.2016.
 */
@RestController
@RequestMapping("/stocks")
public class StockController {

    private Map<ProductEntity, StockProduct> stockProductMap = new HashMap<>();

    @Autowired
    private InputRepository inputRepository;
    @Autowired
    private OutputRepository outputRepository;
    @Autowired
    ProductRepository productRepository;

    private void calculateStock() {
        List<InputEntity> inputEntities = (List<InputEntity>) inputRepository.findAll();
        List<OutputEntity> ouputs = (List<OutputEntity>) outputRepository.findAll();


        stockProductMap.clear();

        for (InputEntity inputEntity : inputEntities){
            StockProduct stockProduct = stockProductMap.get(inputEntity.getProductEntity());
            if (stockProduct == null){
                stockProduct = new StockProduct(inputEntity.getProductEntity(), inputEntity.getStore(), inputEntity.getQuantity());
                stockProductMap.put(inputEntity.getProductEntity(), stockProduct);
            }
            stockProduct.in(inputEntity.getQuantity());
        }


        for (OutputEntity outputEntity : ouputs) {
            StockProduct stockProduct = stockProductMap.get(outputEntity.getProductEntity());
            stockProduct.out(outputEntity.getQuantity());
        }
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<StockProduct>> getStock(){
        calculateStock();
        List<StockProduct> stockProducts = new ArrayList<>(stockProductMap.values());
        Collections.sort(stockProducts, new Comparator<StockProduct>() {
            @Override
            public int compare(StockProduct stockProduct, StockProduct t1) {
                return stockProduct.getProductEntity().getName().compareToIgnoreCase(t1.getProductEntity().getName());
            }
        });
        return ResponseEntity.ok(stockProducts);
    }


}
