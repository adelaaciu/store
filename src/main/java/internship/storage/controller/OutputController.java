package internship.storage.controller;

import internship.storage.model.OutputEntity;
import internship.storage.repository.OutputRepository;
import internship.storage.repository.ProductRepository;
import internship.storage.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by internship on 14.07.2016.
 */
@RestController
@RequestMapping("/outputs")
public class OutputController {


    @Autowired
    private OutputRepository outputRepository;
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreRepository storeRepository;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<OutputEntity>> getAllOutputs() {

        List<OutputEntity> outputEntityList = (List<OutputEntity>) outputRepository.findAll();

        if (outputEntityList.size() > 0)
            return ResponseEntity.ok(outputEntityList);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @RequestMapping("/{id}")
    public ResponseEntity<OutputEntity> getOutput(@PathVariable("id") long id) {

        if (outputRepository.findOne(id) != null)
            return ResponseEntity.ok(outputRepository.findOne(id));

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity addOutput(@RequestBody OutputEntity input) {

      long productId = input.getProductEntity().getId();
        long storeId = input.getStore().getId();
        boolean existProduct = productRepository.exists(productId);
        boolean existStore = storeRepository.exists(storeId);

        if (!existProduct) {

            return (ResponseEntity) ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        if (!existStore) {

            return (ResponseEntity) ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return ResponseEntity.ok(outputRepository.save(input));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity putInput(@PathVariable("id") long id) {
        return (ResponseEntity) ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteInput(@PathVariable("id") long id) {
        return (ResponseEntity) ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public ResponseEntity deleteAllInputs() {
        return (ResponseEntity) ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }
}
