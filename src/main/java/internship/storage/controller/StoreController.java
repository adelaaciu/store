package internship.storage.controller;


import internship.storage.dto.StoreDTO;
import internship.storage.model.StoreEntity;
import internship.storage.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by internship on 13.07.2016.
 */


@RestController
@RequestMapping("/stores")
public class StoreController {

    @Autowired
    private StoreRepository storeRepository;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<StoreDTO>> getAllProducts() {

        List<StoreEntity> storeList = (List<StoreEntity>) storeRepository.findAll();

        List<StoreDTO> storeDTOs = new ArrayList<>();

        for (StoreEntity store : storeList) {
            storeDTOs.add(store.convertToDto());
        }


        if (storeList.size() > 0)
            return ResponseEntity.ok(storeDTOs);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<StoreDTO> getStore(@PathVariable("id") Long id) {


        if (storeRepository.findOne(id) != null)
            return ResponseEntity.ok(storeRepository.findOne(id).convertToDto());

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<StoreDTO> postStore(@RequestBody StoreDTO dto) {

        StoreEntity  store = dto.updateFromDto();
        int size = storeRepository.findByName(dto.getName()).size();
        if (size > 0) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        storeRepository.save(store);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<StoreDTO> updateStore(@PathVariable("id") long id, /*@Valid*/ @RequestBody StoreDTO dto) {

        StoreEntity storeEntity = storeRepository.findOne(id);

        if (storeEntity != null) {
            StoreEntity store = dto.updateFromDto();
            store.setId(id);
            storeRepository.save(store);
            return ResponseEntity.ok(dto);
        }


        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteStore(@PathVariable("id") long id) {

        boolean found = storeRepository.exists(id);

        if (found) {
            storeRepository.delete(id);
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/list", method = RequestMethod.DELETE)
    public ResponseEntity deleteAllStors() {

        if (((List<StoreEntity>) storeRepository.findAll()).size() > 0) {
            storeRepository.deleteAll();
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    private List<StoreDTO> getStoresAsDto() {
        List<StoreDTO> stores = new ArrayList<>();
        for (StoreEntity s : storeRepository.findAll())
            stores.add(s.convertToDto());

        return stores;
    }
}
