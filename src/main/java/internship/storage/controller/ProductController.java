package internship.storage.controller;

import internship.storage.dto.ProductDTO;
import internship.storage.model.ProductEntity;
import internship.storage.model.StoreEntity;
import internship.storage.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by internship on 13.07.2016.
 */

@RestController
@RequestMapping("/products")
public class ProductController {


    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<ProductDTO>> getAllProducts() {

        List<ProductEntity> productEntityList = (List<ProductEntity>) productRepository.findAll();


        List<ProductDTO> productDTOs = new ArrayList<>();

        for (ProductEntity manager : productEntityList) {
            productDTOs.add(manager.convertToDto());
        }

        if (productEntityList.size() > 0)
            return ResponseEntity.ok(productDTOs);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProductDTO> getProduct(@PathVariable("id") Long id) {


        if (productRepository.findOne(id) != null)
            return ResponseEntity.ok(productRepository.findOne(id).convertToDto());

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<ProductDTO> postProduct(@RequestBody ProductDTO dto) {

        ProductEntity p = dto.updateFromDto();
        int size = productRepository.findByName(dto.getName()).size();
        if (size > 0) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        productRepository.save(p);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable("id") long id,/* @Valid*/ @RequestBody ProductDTO dto) {

        ProductEntity productEntity = productRepository.findOne(id);

        if (productEntity != null) {
            ProductEntity product = dto.updateFromDto();
            product.setId(id);
            productRepository.save(product);
            return ResponseEntity.ok(dto);
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteProduct(@PathVariable("id") long id) {

        boolean found = productRepository.exists(id);

        if (found) {
            productRepository.delete(id);
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/list", method = RequestMethod.DELETE)
    public ResponseEntity deleteAllProducts() {

        if (((List<ProductEntity>) productRepository.findAll()).size() > 0) {
            productRepository.deleteAll();
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}
