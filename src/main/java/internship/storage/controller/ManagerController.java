package internship.storage.controller;

import internship.storage.dto.ManagerDTO;
import internship.storage.model.ManagerEntity;
import internship.storage.repository.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by internship on 13.07.2016.
 */

@RestController
@RequestMapping("/managers")
public class ManagerController {


    @Autowired
    private ManagerRepository managerRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<ManagerDTO>> getAllManagers() {


        List<ManagerEntity> managerEntityList = (List<ManagerEntity>) managerRepository.findAll();
        List<ManagerDTO> managerDTOs = new ArrayList<>();

        for (ManagerEntity manager : managerEntityList) {
            managerDTOs.add(manager.convertToDto());
        }


        if (managerEntityList.size() > 0)
            return ResponseEntity.ok(managerDTOs);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/list", method = RequestMethod.DELETE)
    public ResponseEntity deleteAllManagers() {

        if (((List<ManagerEntity>) managerRepository.findAll()).size() > 0) {
            managerRepository.deleteAll();
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping("/{id}")
    public ResponseEntity<ManagerDTO> getManager(@PathVariable("id") long id) {

        if (managerRepository.findOne(id) != null)
            return ResponseEntity.ok(managerRepository.findOne(id).convertToDto());

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity postManager(@RequestBody ManagerDTO dto) {


        int size = managerRepository.findByLastName(dto.getLastName()).size();

        if (size > 0) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        ManagerEntity managerEntity = dto.update();

        managerRepository.save(managerEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ManagerDTO> updateManager(@PathVariable("id") long id,/* @Valid*/ @RequestBody ManagerDTO dto) {


        ManagerEntity manager = managerRepository.findOne(id);

        if (manager != null) {
            ManagerEntity managerEntity = dto.update();
            managerEntity.setId(id);
            managerRepository.save(managerEntity);
            return ResponseEntity.ok(dto);
        }
        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteManager(@PathVariable("id") long id) {

        boolean found = managerRepository.exists(id);

        if (found) {
            managerRepository.delete(id);
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
