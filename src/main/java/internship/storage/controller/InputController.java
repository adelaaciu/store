package internship.storage.controller;


import internship.storage.model.InputEntity;
import internship.storage.repository.InputRepository;
import internship.storage.repository.ProductRepository;
import internship.storage.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by internship on 14.07.2016.
 */
@RestController
@RequestMapping("/inputs")
public class InputController {

    @Autowired
    private InputRepository inputRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreRepository storeRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<InputEntity>> getAllPInputs() {

        List<InputEntity> inputEntityList = (List<InputEntity>) inputRepository.findAll();

        if (inputEntityList.size() > 0)
            return ResponseEntity.ok(inputEntityList);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @RequestMapping("/{id}")
    public ResponseEntity<InputEntity> getInput(@PathVariable("id") long id) {

        if (inputRepository.findOne(id) != null)
            return ResponseEntity.ok(inputRepository.findOne(id));

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity addInput(@RequestBody InputEntity inputEntity) {


        long id = inputEntity.getProductEntity().getId();

        boolean existStore = storeRepository.exists(inputEntity.getStore().getId());
        boolean existProduct = productRepository.exists(inputEntity.getProductEntity().getId());
        if (!existProduct) {

            return (ResponseEntity) ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        if (!existStore) {

            return (ResponseEntity) ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return ResponseEntity.ok(inputRepository.save(inputEntity));

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity putInput(@PathVariable("id") long id) {
        return (ResponseEntity) ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteInput(@PathVariable("id") long id) {
        return (ResponseEntity) ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public ResponseEntity deleteAllInputs() {
        return (ResponseEntity) ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }


}
