package internship.storage;


import internship.storage.model.*;
import internship.storage.repository.*;
import org.apache.catalina.Manager;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;



@SpringBootApplication
public class StoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(ManagerRepository managerRepository,InputRepository inputRepository, OutputRepository outputRepository, ProductRepository productRepository, StoreRepository storeRepository) {
        return (args) -> {
            // save a couple of customers

            ManagerEntity managerEntity = new ManagerEntity("Jack", "Bauer");
            managerRepository.save(managerEntity);


            StoreEntity store = new StoreEntity("Fruits and Vegetables");
            StoreEntity store2 = new StoreEntity("Les Potatos");

            storeRepository.save(store);
            storeRepository.save(store2);

            ProductEntity productEntity = new ProductEntity("apple");
            ProductEntity productEntity1 = new ProductEntity("prod");

            productRepository.save(productEntity);
            productRepository.save(productEntity1);

            inputRepository.save(new InputEntity(productRepository.findOne(productEntity.getId()), store, 15L));
            inputRepository.save(new InputEntity(productRepository.findOne(productEntity1.getId()), store2, 10L));


            outputRepository.save(new OutputEntity(productRepository.findOne(productEntity.getId()), store, 6L));
            outputRepository.save(new OutputEntity(productRepository.findOne(productEntity1.getId()), store, 5L));
        };
    }
}
