package internship.storage.dto;

import internship.storage.model.StoreEntity;

/**
 * Created by internship on 15.07.2016.
 */
public class StoreDTO {

    private String name;

    protected StoreDTO(){

    }

    public StoreDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StoreEntity updateFromDto(){
        return  new StoreEntity(this.getName());
    }
}
