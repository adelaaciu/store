package internship.storage.dto;

import internship.storage.model.ManagerEntity;

import java.io.Serializable;


/**
 * Created by internship on 15.07.2016.
 */
public class ManagerDTO implements Serializable{

    private String firstName;
    private String lastName;


    protected ManagerDTO(){

    }
    public ManagerDTO(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ManagerEntity update(){
        return new ManagerEntity(this.getFirstName(),this.getLastName());
    }

}
