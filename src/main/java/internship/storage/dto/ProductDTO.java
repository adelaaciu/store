package internship.storage.dto;

import internship.storage.model.ProductEntity;

/**
 * Created by internship on 15.07.2016.
 */
public class ProductDTO {

    private String name;
    protected ProductDTO(){

    }

    public ProductDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductEntity updateFromDto(){
        return  new ProductEntity(this.getName());
    }
}
