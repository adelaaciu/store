package internship.storage.model;

import com.google.gson.Gson;
import internship.storage.dto.ManagerDTO;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Created by internship on 13.07.2016.
 */
@Entity
@Table(name = "managers")
public class ManagerEntity extends BaseEntity {


    @Column(name = "first_name", nullable = false)
    @NotEmpty(message = "Required field")
    private String firstName;

    @NotEmpty(message = "Required field")
    @Column(name = "last_name", nullable = false)
    private String lastName;

    protected ManagerEntity() {

    }

    public ManagerEntity(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String toJsonString(ManagerEntity managerEntity) {
        Gson gson = new Gson();
        return gson.toJson(managerEntity);

    }

    public ManagerDTO convertToDto() {

        return new ManagerDTO(this.getFirstName(), this.getLastName());
    }

    @Override
    public String toString() {
        return "ManagerEntity{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
