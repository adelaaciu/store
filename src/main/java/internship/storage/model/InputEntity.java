package internship.storage.model;


import com.google.gson.Gson;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "inputs")
public class InputEntity extends BaseEntity {

    @NotNull(message = "ProductEntity is null")
    @JoinColumn(name = "product_id")
    @ManyToOne
    private ProductEntity productEntity;

    @JoinColumn(name = "store_id")
    @NotNull(message = "StoreEntity is null")
    @ManyToOne
    private StoreEntity store;

    @Column(name = "quantity_id")
    @NotNull
    private long quantity;

    protected InputEntity() {

    }

    public InputEntity(ProductEntity productEntity, StoreEntity store, long quantity) {

        this.productEntity = productEntity;
        this.store = store;
        this.quantity = quantity;
    }


    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }


    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }


    public String toJSon(InputEntity inputEntity) {

        Gson gson = new Gson();
        return gson.toJson(inputEntity);
    }

}
