package internship.storage.model;


import internship.storage.dto.StoreDTO;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by internship on 13.07.2016.
 */
@Entity
@Table(name = "stores")
public class StoreEntity extends BaseEntity {


    @NotEmpty
    @Column(name = "store_name", nullable = false, unique = true)
    private String name;

    protected StoreEntity() {
    }

    public StoreEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StoreDTO convertToDto() {
        return new StoreDTO(this.getName());
    }

}
