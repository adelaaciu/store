package internship.storage.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by internship on 14.07.2016.
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
