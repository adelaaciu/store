package internship.storage.model;


import internship.storage.dto.ProductDTO;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Created by internship on 13.07.2016.
 */
@Entity
@Table(name = "products")
public class ProductEntity extends BaseEntity {

    @NotEmpty
    @Column(name = "product_name", nullable = false, unique = true)
    private String name;

    protected ProductEntity() {
    }

    public ProductEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductEntity productEntity = (ProductEntity) o;

        return name != null ? name.equals(productEntity.name) : productEntity.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public ProductDTO convertToDto(){
        return  new ProductDTO(this.getName());
    }

}
