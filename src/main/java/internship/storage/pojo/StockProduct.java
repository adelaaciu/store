package internship.storage.pojo;

import internship.storage.model.ProductEntity;
import internship.storage.model.StoreEntity;


/**
 * Created by internship on 14.07.2016.
 */
public class StockProduct {
    private ProductEntity productEntity;
    private long quantity;
    private StoreEntity store;

    public StockProduct(ProductEntity productEntity, StoreEntity store, long quantity) {
        this.productEntity = productEntity;
        this.store = store;
        this.quantity = quantity;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public long in(long quant) {
        this.quantity = this.quantity + quant;
        return this.quantity;
    }

    public long out(long quant) {
        this.quantity = this.quantity - quant;
        return this.quantity;
    }

    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }


    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

}
