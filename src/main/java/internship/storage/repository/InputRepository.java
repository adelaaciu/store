package internship.storage.repository;

import internship.storage.model.InputEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by internship on 14.07.2016.
 */
public interface InputRepository  extends PagingAndSortingRepository<InputEntity,Long> {


}
