package internship.storage.repository;

import internship.storage.model.ManagerEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by internship on 13.07.2016.
 */
public interface ManagerRepository  extends CrudRepository<ManagerEntity,Long> {

    List<ManagerEntity> findByLastName(String name);
}
