package internship.storage.repository;

import internship.storage.model.OutputEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by internship on 14.07.2016.
 */
public interface OutputRepository  extends CrudRepository<OutputEntity, Long> {

}
