package internship.storage.repository;

import internship.storage.model.ProductEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository extends PagingAndSortingRepository<ProductEntity,Long> {

    List<ProductEntity> findByName(String name);
}
