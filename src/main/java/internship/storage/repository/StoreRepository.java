package internship.storage.repository;


import internship.storage.model.StoreEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by internship on 13.07.2016.
 */
public interface StoreRepository extends CrudRepository<StoreEntity, Long> {

    List<StoreEntity> findByName(String name);

}
